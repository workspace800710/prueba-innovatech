import React, { useEffect, useState } from 'react';

function TodoApp() {
  const [tasks, setTasks] = useState([]);
  const [taskInput, setTaskInput] = useState('');

  const getTask = async () => {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((response) => response.json())
      .then((restTask) => {
        console.log(restTask)
        setTasks(restTask)
      });
  }

  const addTask = () => {
    if (taskInput.trim() !== '') {
      fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
          title: taskInput,
          body: 'Prueba innovatech',
          userId: 99999999,
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      })
        .then((response) => response.json())
        .then((newTask) => {
          setTasks([...tasks, newTask]);
          setTaskInput('');
          alert("Tarea guardada exitosamente");
        });
    }
  };


  const deleteTask = (taskId) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${taskId}`, {
      method: 'DELETE',
    }).then(() => { alert("Tarea eliminada exitosamente") });
    setTasks(tasks.filter((task) => task.id !== taskId));
  };

  const editTask = (taskId, newName) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${taskId}`, {
      method: 'PUT',
      body: JSON.stringify({
        id: taskId,
        title: newName,
        body: 'Prueba innovatech',
        userId: 99999999,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((updateTask) => {
        console.log(updateTask)
        setTasks(tasks.map(task => {
          if (task.id === taskId) {
            return { ...task, title: newName };
          }
          return task;
        }));
        alert("Tarea actualizada exitosamente")
      });
  };


  useEffect(() => {
    getTask()
  }, [])


  return (
    <div className='bg-slate-300 w-screen h-full'>
      <div className="flex justify-center flex-col container mx-auto py-8">
        <h1 className="bg-blue-300 text-4xl font-bold text-center mb-8 text-white rounded-md">All task</h1>

        <div className="flex items-center justify-center mb-8">
          <input
            type="text"
            placeholder="Enter a task"
            className="bg-white w-1/2 border rounded py-2 px-3 mb-3 focus:outline-none focus:ring-2 focus:ring-blue-500"
            value={taskInput}
            onChange={(e) => setTaskInput(e.target.value)}
          />
          <button
            className="bg-blue-500 text-white py-2 px-4 rounded ml-2 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500"
            onClick={addTask}
          >
            Add Task
          </button>
        </div>

        <table className="table-fixed ">
          <thead>
            <tr className="bg-blue-300">
              <th className="px-4 py-2 text-left">Task</th>
              <th className="px-4 py-2 text-left">Actions</th>
            </tr>
          </thead>
          <tbody>
            {tasks.map(task => (
              <tr key={task.id} className="bg-blue-50">
                <td className="border px-4 py-2 text-gray-800">{task.title}</td>
                <td className="border px-4 py-2">
                  <button
                    className="bg-yellow-500 text-white py-1 px-2 rounded mr-2 hover:bg-yellow-600 focus:outline-none focus:ring-2 focus:ring-yellow-500"
                    onClick={() => {
                      const newName = prompt("Enter new task name:", task.title);
                      if (newName !== null) {
                        editTask(task.id, newName.trim());
                      }
                    }}
                  >
                    Edit
                  </button>
                  <button
                    className="bg-red-500 text-white py-1 px-2 rounded hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-red-500"
                    onClick={() => deleteTask(task.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TodoApp;
